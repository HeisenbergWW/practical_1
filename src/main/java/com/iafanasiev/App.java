package com.iafanasiev;

import java.util.Properties;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonAppend;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {

    static Logger logger = LoggerFactory.getLogger(App.class);
    static Logger helloLogger = LoggerFactory.getLogger("helloLogger");
    private static final String PROPERTIES_NAME = "app.properties";

    public static void main(String[] args) {
        try {
            logger.info("Start of program");

            String format = System.getProperty("format", "json");
            logger.debug("Got system property \"format\" - {}", format);

            Properties properties = PropertiesLoader.loadProperties(PROPERTIES_NAME);

            String username = properties.getProperty("username");
            logger.debug("Got system property \"username\" - {}", username);
            properties.clear();

            String message = "Привіт " + username + "!";

            ObjectMapper objectMapper;
            if ("xml".equalsIgnoreCase(format)) {
                logger.info("Converting to XML");
                objectMapper = new XmlMapper();

            } else {
                logger.info("Converting to JSON");
                objectMapper = new ObjectMapper();
            }
            ObjectNode objectNode = objectMapper.createObjectNode();
            objectNode.put("message", message);

            String textMessage = objectMapper.writeValueAsString(objectNode);
            helloLogger.info(textMessage);
            logger.info("result: {}", textMessage);
        } catch (Exception e) {
            logger.error("Error converting", e);
        }
        logger.info("End of program");
    }
}