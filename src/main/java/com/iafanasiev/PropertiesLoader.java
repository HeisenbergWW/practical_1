package com.iafanasiev;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

public class PropertiesLoader {
    private static final Logger logger = LoggerFactory.getLogger(PropertiesLoader.class);

    public static Properties loadProperties(String fileName) throws IOException {
        Properties properties = new Properties();

        logger.debug("Get the stream from the {} file through the class-loader", fileName);
        InputStream inputStream = PropertiesLoader.class.getClassLoader().getResourceAsStream(fileName);

        logger.debug("Load properties");
        assert inputStream != null;
        properties.load(new InputStreamReader(inputStream, StandardCharsets.UTF_8));

        return properties;
    }

    private PropertiesLoader() {
    }
}